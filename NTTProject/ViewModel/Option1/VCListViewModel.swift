//
//  VCListViewModel.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Alamofire
import RxSwift

final class VCListViewModel  {
    
    let loading = PublishSubject<Bool>()
    let results = PublishSubject<[ResultListMovieResponse]>()
    
    
    func fetchData(){
        let url = Constants.baseUrlOption2 + "/3/discover/movie?api_key=" + Constants.keyOption2 + "&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1"
        
        showLoading()
        
        AF.request( url ).responseJSON { response in
            
            switch response.result {
            case .success(let data):
                self.disableLoading()
                if let json = data as? [String: Any] {
                    let listMovieResponse = ListMovieResponse(response : json)
                    self.results.onNext(listMovieResponse.result)
                }
                
            case .failure(let error):
                self.disableLoading()
                print("\n Failure: \(error.localizedDescription)")
            }
        }
    }
    
    func showLoading(){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
            self.loading.onNext(true)
        }
    }
    
    func disableLoading(){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
            self.loading.onNext(false)
        }
    }
}
