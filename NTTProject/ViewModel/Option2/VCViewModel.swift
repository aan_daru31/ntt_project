//
//  VCViewModel.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//



import Foundation
import Alamofire
import RxSwift

final class VCViewModel  {
    
    let loading = PublishSubject<Bool>()
    let dataSourches = PublishSubject <[ItemListParentREsponse]>()
    
   

    func fetchData(){
        let url =  Constants.baseUrlOption1 + "/sources?apiKey=" + Constants.keyOption1

        showLoading()

        AF.request( url ).responseJSON { response in

            switch response.result {
            case .success(let data):
                self.disableLoading()
                if let json = data as? [String: Any] {
                    let listParentResponse = ListParentREsponse(response: json)
//                        self.sources.onNext(sources)
                    self.dataSourches.onNext(listParentResponse.sources)
                }

            case .failure(let error):
                self.disableLoading()
                print("\n Failure: \(error.localizedDescription)")
            }
        }
    }
    
    func showLoading(){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
            self.loading.onNext(true)
        }
    }
    
    func disableLoading(){
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
                   self.loading.onNext(false)
               }
    }
}
