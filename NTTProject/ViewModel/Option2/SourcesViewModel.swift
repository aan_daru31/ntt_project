//
//  SourcesViewModel.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

final class SourcesViewModel  {
    
    let loading = PublishSubject<Bool>()
    let dataArticle = PublishSubject <[ArticlesResponse]>()
    var category : String?
    var country : String?
    
    func fetchData(){
        
        let url = Constants.baseUrlOption1 + "/top-headlines?country=" + country! + "&category=" + category! + "&apiKey=" + Constants.keyOption1
        
        showLoading()
        
        AF.request( url ).responseJSON { response in
            
            switch response.result {
            case .success(let data):
                self.disableLoading()
                if let json = data as? [String: Any] {
                    let sourceResponse = SourceResponse(response: json)
                    self.dataArticle.onNext(sourceResponse.articles)
                }
                
            case .failure(let error):
                self.disableLoading()
                print("\n Failure: \(error.localizedDescription)")
            }
        }
    }
    
    func showLoading(){
           DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
               self.loading.onNext(true)
           }
       }
       
       func disableLoading(){
           DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
                      self.loading.onNext(false)
                  }
       }
    
}
