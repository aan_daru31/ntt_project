//
//  Cooridinator.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.

import Foundation
import UIKit

class Cooridinator {
    
    static func sourceView(selfView: UIViewController, category : String, country : String) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SourcesViewController") as! SourcesViewController
        let viewModel =  SourcesViewModel.init()
        viewModel.category            = category
        viewModel.country             = country
        popOverVC.viewModel = viewModel
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle   = UIModalTransitionStyle.crossDissolve
        selfView.present(popOverVC, animated: true, completion: nil)
    }
    
    static func webViewController(selfView: UIViewController, url : String) {
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        popOverVC.urlWeb = url
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            popOverVC.modalTransitionStyle   = UIModalTransitionStyle.crossDissolve
        
            selfView.present(popOverVC, animated: true, completion: nil)
        }
    
    static func moviewInfoScreenViewController(selfView: UIViewController, resultListMovieResponse : ResultListMovieResponse) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoviewInfoScreenViewController") as! MoviewInfoScreenViewController
        let viewModel =  MoviewInfoScreenViewModel.init()
        viewModel.resultListMovieResponse = resultListMovieResponse
        popOverVC.viewModel = viewModel
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        popOverVC.modalTransitionStyle   = UIModalTransitionStyle.crossDissolve
        selfView.present(popOverVC, animated: true, completion: nil)
    }
    
    
    
    
}

extension UIImageView {
    func loadImge(withUrl url: URL) {
        let cache =  URLCache.shared
        let request = URLRequest(url: url)
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.transition(toImage: image)
                }
            } else {
                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
                        let cachedData = CachedURLResponse(response: response, data: data)
                        cache.storeCachedResponse(cachedData, for: request)
                        DispatchQueue.main.async {
                            self.transition(toImage: image)
                        }
                    }
                }).resume()
            }
        }
    }
    
    public func transition(toImage image: UIImage?) {
        UIView.transition(with: self, duration: 0.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.image = image
        },
                          completion: nil)
    }
}
