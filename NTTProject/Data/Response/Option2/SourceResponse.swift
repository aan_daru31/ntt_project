//
//  SourceResponse.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
class SourceResponse: Codable {
    var status     : String?
    var totalResults : Int?
    var articles    : [ArticlesResponse] = []
    
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        self.status    =  response["name"] as? String
        self.totalResults    =  response["totalResults"] as? Int
        let articles    = response["articles"] as! [Any]
        
        var items : [ArticlesResponse] = []
               
               
               for data in articles {
                   let articlesResponse = ArticlesResponse.init(response: data as? [String : Any])
                       items.append(articlesResponse)
                       }
               self.articles = items
       
    }
}

class ArticlesResponse: Codable {
    var title     : String?
    var description : String?
    var urlToImage : String?
    var publishedAt : String?
    var url : String?
    
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        title = response["title"] as? String
        description = response["description"] as? String
        urlToImage = response["urlToImage"] as? String
        publishedAt = response["publishedAt"] as? String
        url = response["url"] as? String
       
    }
}
