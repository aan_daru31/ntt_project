//
//  ListParentREsponse.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
class ListParentREsponse: Codable {
    var status     : String?
    var sources    : [ItemListParentREsponse] = []
    
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        self.status    =  response["name"] as? String
        let sources    = response["sources"] as! [Any]
        
        var items : [ItemListParentREsponse] = []
               
               
               for data in sources {
                   let itemListParentREsponse = ItemListParentREsponse.init(response: data as? [String : Any])
                       items.append(itemListParentREsponse)
                       }
               self.sources = items
       
    }
}

class ItemListParentREsponse: Codable {
    var id     : String?
    var name   : String?
    var description : String?
    var url : String?
    var category : String?
    var language : String?
    var country : String?
    
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        id = response["id"] as? String
        name = response["name"] as? String
        description = response["description"] as? String
        url = response["url"] as? String
        category = response["category"] as? String
        language = response["language"] as? String
        country = response["country"] as? String
       
    }
}
