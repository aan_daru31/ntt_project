//
//  ListMovieResponse.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
class ListMovieResponse: Codable {
    var status     : String?
    var total_results : Int?
    var total_pages : Int?
    var result    : [ResultListMovieResponse] = []
    
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        self.status    =  response["name"] as? String
        self.total_results    =  response["total_results"] as? Int
        self.total_pages    =  response["total_pages"] as? Int
        let results    = response["results"] as! [Any]
        
        var items : [ResultListMovieResponse] = []
               
               
               for data in results {
                   let resultListMovieResponse = ResultListMovieResponse.init(response: data as? [String : Any])
                       items.append(resultListMovieResponse)
                       }
               self.result = items
       
    }
}

class ResultListMovieResponse: Codable {
    var title     : String?
    var poster_path   : String?
    var vote_average : Double?
    var overview : String?
    
    
    init(response: [String: Any]?) {
        
        guard let response = response else {
            return
        }
        title = response["title"] as? String
        poster_path = response["poster_path"] as? String
        vote_average = response["vote_average"] as? Double
        overview = response["overview"] as? String
       
    }
}
