//
//  MoreInfoScreenCell.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
import UIKit
import YoutubePlayer_in_WKWebView
import Cosmos

class MoreInfoScreenCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var viewPlayer: WKYTPlayerView!
    @IBOutlet weak var ratingsView: CosmosView!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        
    }
}
