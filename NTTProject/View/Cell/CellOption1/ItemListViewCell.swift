//
//  ItemListViewCell.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import UIKit

class ItemListViewCell: UICollectionViewCell {
    @IBOutlet weak var viewItem: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    
}
