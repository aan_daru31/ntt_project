//
//  SourcesCell.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
import UIKit

class SourcesCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArticle: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        imgArticle.layer.cornerRadius = 8.0
    }
}
