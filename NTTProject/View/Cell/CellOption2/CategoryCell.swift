//
//  CategoryCell.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
import UIKit

class CategoryCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        viewBackground.layer.cornerRadius = 8.0
    }
}
