//
//  ListMoviewViewController.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
import UIKit
class ListMoviewViewController: UIViewController {
    @IBOutlet weak var collectionView   : UICollectionView!
    var viewModel : VCListViewModel?
    var result: [ResultListMovieResponse] = []
     let alertLoading = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    
    override func viewDidLoad() {
    super.viewDidLoad()
        setupUICollection()
        viewModel = VCListViewModel.init()
        handleData()
    }
}

extension ListMoviewViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    private func setupUICollection(){
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 2
            let heights = 16
            layout.sectionInset = UIEdgeInsets(top: CGFloat(heights) , left: 10, bottom: 0, right: 10)
            let size = CGSize(width: self.view.frame.size.width / 2.2 , height: 150)
            layout.itemSize = size
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.result.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemListViewCell", for: indexPath) as! ItemListViewCell
        cell.lbl.text = result[indexPath.row].title
        
        
        if (((result[indexPath.row].poster_path)) != nil) {
            let imageUrl:URL = URL(string:  ("https://image.tmdb.org/t/p/original" + (result[indexPath.row].poster_path)!) )!
        cell.imageView.loadImge(withUrl: imageUrl)
         } else {
             let imageUrl:URL = URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfw63-L7EbCFKbpVihu0CZW-PJAdmcXvneYkbaVUe-E9Gig7KstQ&s")!
             cell.imageView.loadImge(withUrl: imageUrl)
         }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let displayData = eventInOtherCityListEvent[indexPath.row]
        Cooridinator.moviewInfoScreenViewController(selfView: self, resultListMovieResponse: result[indexPath.row])
    }
    
   
}

extension ListMoviewViewController {
    
    func handleData() {
        viewModel?.fetchData()
        
        viewModel?.loading.subscribe { (response) in
            if response.element! {
                self.showLoading()
            } else {
                self.disableLoading()
            }
        }
        
        viewModel?.results.subscribe { (response) in
            self.result = response.element!
            self.collectionView.reloadData()
        }
    }
}

extension ListMoviewViewController {
    
    func showLoading(){

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alertLoading.view.addSubview(loadingIndicator)
        DispatchQueue.main.async{
            self.present(self.alertLoading, animated: true, completion: nil)
        }
    }

    func disableLoading(){
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when){
            self.alertLoading.dismiss(animated: true, completion: nil)
        }
    }
//
    
}
