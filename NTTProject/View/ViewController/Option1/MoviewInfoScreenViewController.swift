//
//  MoviewInfoScreenViewController.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import UIKit

class MoviewInfoScreenViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var viewModel : MoviewInfoScreenViewModel?
    override func viewDidLoad() {
    super.viewDidLoad()
        setupUITable()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - Setup Table
extension MoviewInfoScreenViewController: UITableViewDataSource, UITableViewDelegate {
    private func setupUITable(){
        
        self.tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 579 //UITableView.automaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nib = UINib(nibName: "MoreInfoScreenCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MoreInfoScreenCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoScreenCell", for: indexPath) as! MoreInfoScreenCell
        cell.viewPlayer.load(withVideoId: "nxi6rtBtBM0")
        cell.lblTitle.text = viewModel!.resultListMovieResponse!.title
        cell.lblDesc.text = viewModel!.resultListMovieResponse!.overview
     
        return cell
        
    }
    
}
