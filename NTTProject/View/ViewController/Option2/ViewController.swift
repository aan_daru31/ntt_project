//
//  ViewController.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var tvSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var viewModel : VCViewModel!
    var dataSource: [ItemListParentREsponse] = []
    var filterSources : [ItemListParentREsponse] = []
    let alertLoading = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    
    var loadMore = 10

    override func viewDidLoad() {
        super.viewDidLoad()
        tvSearch.delegate = self
        viewModel = VCViewModel.init()
        setupUITable()
        handleData()
        
       
    }
    
    // UITextFieldDelegate
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          self.view.endEditing(true)
        if tvSearch.text != "" {
            filterSources = dataSource.filter{ $0.name!.localizedStandardContains(tvSearch.text!)}
            tableView.reloadData()
        } else {
            filterSources = dataSource
            tableView.reloadData()
        }
          return false
      }


}

//MARK: - Setup Table
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    private func setupUITable(){
        
        self.tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 153 //UITableView.automaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nib = UINib(nibName: "CategoryCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "CategoryCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
       
//        let data = dataCity[indexPath.row]
        cell.lblTitle.text = (filterSources[indexPath.row].name as! String)
        cell.lblDesc.text = (filterSources[indexPath.row].description as! String)
        cell.lblCategory.text = "Category : " + (filterSources[indexPath.row].category as! String)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Cooridinator.sourceView(selfView: self, category : filterSources[indexPath.row].category as! String, country : filterSources[indexPath.row].country as! String)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let alert = UIAlertController(title: "No More Data", message: "Total data " + String(indexPath.row + 1), preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                           switch action.style{
                           case .default:
                               print("default")
                               
                           case .cancel:
                               print("cancel")
                               
                           case .destructive:
                               print("destructive")
                           }
                       
                       }))
        }
    }
    
}

//MARK: - Handle Data
extension ViewController {
    
    func handleData() {
        viewModel.fetchData()
        
        viewModel.loading.subscribe { (response) in
            if response.element! {
                self.showLoading()
            } else {
                self.disableLoading()
            }
        }
        
        viewModel.dataSourches.subscribe { (response) in
            self.dataSource = response.element ?? []
            self.filterSources = response.element ?? []
            self.tableView.reloadData()
        }
    }
}

//MARK: - Setup Loading
extension ViewController {
    
    func showLoading(){

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alertLoading.view.addSubview(loadingIndicator)
        DispatchQueue.main.async{
            self.present(self.alertLoading, animated: true, completion: nil)
        }
    }

    func disableLoading(){
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when){
            self.alertLoading.dismiss(animated: true, completion: nil)
        }
    }
//
    
}

