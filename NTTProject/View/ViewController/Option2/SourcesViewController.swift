//
//  SourcesViewController.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import UIKit
import Alamofire

class SourcesViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var tvSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var viewModel : SourcesViewModel!
    var sources : [ArticlesResponse] = []
    var filterSources : [ArticlesResponse] = []
    let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tvSearch.delegate = self
        setupUITable()
        handleData()
    }
    
    // UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
      if tvSearch.text != "" {
        filterSources = sources.filter{ $0.title!.localizedStandardContains(tvSearch.text!)}
          tableView.reloadData()
      } else {
          filterSources = sources
          tableView.reloadData()
      }
        return false
    }

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Handle Data
extension SourcesViewController {
    
    func handleData() {
        viewModel.fetchData()
        
        viewModel.loading.subscribe { (response) in
            if response.element! {
                self.showLoading()
            } else {
                self.disableLoading()
            }
        }
        
        viewModel.dataArticle.subscribe { (response) in
            self.sources = response.element!
            self.filterSources = response.element!
            self.tableView.reloadData()
        }
    }
}

//MARK: - Setup Table
extension SourcesViewController: UITableViewDataSource, UITableViewDelegate {
    
   
    
    private func setupUITable(){
        
        self.tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 439 //UITableView.automaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nib = UINib(nibName: "SourcesCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SourcesCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "SourcesCell", for: indexPath) as! SourcesCell
        
        cell.lblTitle.text = (filterSources[indexPath.row].title as! String)
        cell.lblDesc.text = ((filterSources[indexPath.row].description) as? String)
        cell.lblDate.text = "Published At : " + ((sources[indexPath.row].publishedAt as? String)!)

        if (((sources[indexPath.row].urlToImage)) != nil) {
            let imageUrl:URL = URL(string: ((filterSources[indexPath.row].urlToImage)!) )!
            cell.imgArticle.loadImge(withUrl: imageUrl)
             } else {
                 let imageUrl:URL = URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfw63-L7EbCFKbpVihu0CZW-PJAdmcXvneYkbaVUe-E9Gig7KstQ&s")!
                 cell.imgArticle.loadImge(withUrl: imageUrl)
             }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Cooridinator.webViewController(selfView: self, url : filterSources[indexPath.row].url!)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let alert = UIAlertController(title: "No More Data", message: "Total data " + String(indexPath.row + 1), preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                           switch action.style{
                           case .default:
                               print("default")
                               
                           case .cancel:
                               print("cancel")
                               
                           case .destructive:
                               print("destructive")
                           }
                       
                       }))
        }
    }
    
    
}

//MARK: - Setup Move
extension SourcesViewController {
    
    func showLoading(){

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        DispatchQueue.main.async{
            self.present(self.alert, animated: true, completion: nil)
        }
    }

    func disableLoading(){
        let when = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: when){
            self.alert.dismiss(animated: true, completion: nil)
        }
    }
   
}
