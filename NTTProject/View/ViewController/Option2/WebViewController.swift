//
//  WebViewController.swift
//  NTTProject
//
//  Created by Akhmad Andaru on 13/01/21.
//  Copyright © 2020 Akhmad Andaru. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class WebViewController: UIViewController {
    @IBOutlet weak var viewWeb: UIView!
    var urlWeb = ""
    override func viewDidLoad() {
    super.viewDidLoad()
    let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.viewWeb.addSubview(webView)
    
    let url = URL(string: urlWeb)
    webView.load(URLRequest(url: url!))
    webView.allowsBackForwardNavigationGestures = true
    }
    @IBAction func btnBack(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
}
